import { dataGlasses } from "./controller.js";

let imgHTML = "";

// glassesInfor
function showGlasses(glass) {
  document.getElementById(`avatar`).innerHTML = "";
  document.getElementById(`glassesInfo`).innerHTML = "";
  console.log(glass);
  if (glass) {
    const imgElement = document.createElement("img");
    imgElement.setAttribute("id", "img_remove");
    imgElement.src = glass.virtualImg;
    let a = document.getElementById(`avatar`).appendChild(imgElement);

    for (let i = 0; i <= dataGlasses.length; i++) {
      document.getElementById(`glassesInfo`).style.display = "block";

      const inforElement = `
       <h5>${glass.name} - ${glass.brand} (${glass.color})</h5>
       <p class="price"><span>$${glass.price}</span> Stocking</p>
    
       <p class="desc">${glass.description}</p>
       `;

      console.log("inforElement: ", inforElement);

      let b = (document.getElementById(`glassesInfo`).innerHTML = infoElement);
      return a, b;
    }
  }
}

for (let i = 0; i <= dataGlasses.length; i++) {
  const glass = dataGlasses[i];
  if (glass) {
    const btnContent = `
    <button id="${glass.id}"><img src="${glass.src}" /></button>
    `;

    imgHTML += btnContent;
    document.getElementById(`vglassesList`).innerHTML = imgHTML;
  }
}

for (let i = 0; i <= dataGlasses.length; i++) {
  const glass = dataGlasses[i];
  if (glass) {
    document.getElementById(glass.id).addEventListener("click", (event) => {
      showGlasses(glass);
    });
  }
}

document.getElementById("before").addEventListener("click", (event) => {
  document.getElementById(`img_remove`).style.display = "none";
});

document.getElementById("after").addEventListener("click", (event) => {
  return (document.getElementById(`img_remove`).style.display = "block");
});
